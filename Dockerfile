FROM quay.io/centos/centos:stream8

ENV GO_DOWNLOAD_URI="https://go.dev/dl/go1.18.1.linux-amd64.tar.gz" \
    PATH="/root/.local/bin:/root/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/snap/bin:/var/lib/snapd/snap/bin:/sbin:/usr/local/go/bin:/opt/go/bin" \
    PKG_CONFIG_PATH="/usr/local/lib" \
    GOROOT=/usr/local/go \
    GOPATH=/opt/go \
    CGO_CFLAGS="-I/root/go/deps/raft/include/ -I/root/go/deps/dqlite/include/" \
    CGO_LDFLAGS="-L/root/go/deps/raft/.libs -L/root/go/deps/dqlite/.libs/" \
    LD_LIBRARY_PATH="/root/go/deps/raft/.libs/:/root/go/deps/dqlite/.libs/" \
    CGO_LDFLAGS_ALLOW="(-Wl,-wrap,pthread_create)|(-Wl,-z,now)"

RUN dnf -y install dnf-plugins-core && dnf config-manager --set-enabled -y powertools \
    && dnf -y install git autoconf automake libtool libuv libuv-devel lz4-libs lz4-devel acl libacl-devel attr dnsmasq file shadow-utils libacl libcap libcap-devel libseccomp sqlite sqlite-devel shadow-utils ebtables rsync squashfs-tools tcl xz pkg-config libudev-devel make
    
# Prepare GO

RUN cd /tmp && mkdir -p /opt/go && curl -LO ${GO_DOWNLOAD_URI} \
    && tar -C /usr/local -xzf ./go1*tar.gz \
    && rm ./go1*tar.gz

# Build LXC

ADD ./lxc ./lxc
RUN cd ./lxc && ./autogen.sh \
    && ./configure --prefix=/usr \
    && make \
    && make install \
    && libtool --finish /usr/local/lib \
    && ldconfig -n /usr/local/lib

# Build raft

ADD ./raft ./raft
RUN cd ./raft && autoreconf -i \
    && ./configure --prefix=/usr \
    && make  \
    && make install \
    && libtool --finish /usr/local/lib \
    && ldconfig -n /usr/local/lib 


# build dqlite
ADD ./dqlite ./dqlite
RUN cd ./dqlite \
    && autoreconf -i \
    && ./configure --prefix=/usr \
    && make \
    && make install


ADD ./lxd ./lxd 
RUN cd ./lxd \
    && make deps \
    && make


